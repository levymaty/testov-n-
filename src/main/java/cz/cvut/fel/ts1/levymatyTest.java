package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class levymatyTest {

    @Test
    public void factorialTest() {

        levymaty fac = new levymaty();

        assertEquals(1, fac.factorial(1));
        assertEquals(1, fac.factorial(0));
        assertEquals(120, fac.factorial(5));

        assertEquals(6, fac.factorial(3));
        assertTrue(true);
    }
}
